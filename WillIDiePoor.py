import cherrypy
import json
import mako.lookup
import mako.template
import numpy as np
import os

import CombinedExpectancy
import WealthExpectancy


class Application(object):


	def __init__(self, template_dir):
		self.template_dir = template_dir
		with open(os.path.join(template_dir, "config.json")) as template_config_file:
			self.template_config = json.load(template_config_file)
		# Read the file that contains the parameters for the models that generates returs
		with open("simulated_returns.parameters.json") as parameters_file:
			self.returns_models_parameters = json.load(parameters_file)
			self.returns_generator_provider = lambda : WealthExpectancy.get_returns_generator(self.returns_models_parameters)
		self.simulated_returns = np.load("simulated_returns.npy", allow_pickle=True)


	@cherrypy.expose
	@cherrypy.tools.allow(methods=["GET"])
	def get_future_returns(self):
		print(self.simulated_returns.shape)
		return json.dumps(self.simulated_returns.tolist())


	@cherrypy.expose
	@cherrypy.tools.allow(methods=["POST"])
	def get_wealth_expectancy(self, initial_net_worth, phases, life_expectancy):
		phases = json.loads(phases)
		initial_net_worth = int(initial_net_worth)
		life_expectancy = float(life_expectancy)

		# Returns generated at runtime
		#simulator = WealthExpectancy.SimulatedReturns(CombinedExpectancy.TRIALS, self.returns_generator_provider)

		# Returns previously generated, calculated sequentially
		#simulator = WealthExpectancy.CalculatedReturnsSequential(self.simulated_returns, CombinedExpectancy.TRIALS)

		# Returns previously generated with vectorized calculations
		simulator = WealthExpectancy.CalculatedReturns(self.simulated_returns)

		wealth_expectancy = CombinedExpectancy.get_expectancy_calculations(initial_net_worth, phases, life_expectancy, simulator, np.inf)

		"""
		print("=========================================")
		print("Initial net worth:", initial_net_worth)
		print("Life expectancy:", life_expectancy)
		print("Models parameters:", self.returns_models_parameters)
		print("Phases:", phases)
		print("Results:", { k: v for k, v in wealth_expectancy.items() if k != "WealthExpectancy" })
		print("=========================================")
		print()
		"""

		return json.dumps(wealth_expectancy)


	@cherrypy.expose
	def index(self):
		return self.default("home")


	@cherrypy.expose
	def default(self, page):
		page_config = self.template_config[page]
		return mako.template.Template(
			filename=os.path.join(self.template_dir, page + ".htm"),
			lookup=mako.lookup.TemplateLookup(directories=["template"])
		).render(active = page,
			menu = { k: self.template_config[k]["name"] for k in self.template_config },
			scripts = page_config["scripts"] if "scripts" in page_config else [],
			stylesheets = page_config["stylesheets"] if "stylesheets" in page_config else [],
			body_attributes = page_config["body_attributes"] if "body_attributes" in page_config else "",
			returns_models_parameters = self.returns_models_parameters,
			trials = self.simulated_returns.shape[0]
		)


def Start(socket_host, socket_port):

	static_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "static"))
	template_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "template"))

	cherrypyconfig = {
		"/static" : {
			"tools.staticdir.on": True,
			"tools.staticdir.dir": static_dir,
			"tools.staticdir.index": "index.htm",
		},
	}
	cherrypy.server.socket_host = socket_host
	cherrypy.server.socket_port = socket_port
	cherrypy.quickstart(Application(template_dir), config=cherrypyconfig)
