import argparse

import WillIDiePoor


if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("--socket_host", required=False, default="127.0.0.1")
	parser.add_argument("--socket_port", required=False, type=int, default=8080)
	args = parser.parse_args()

	WillIDiePoor.Start(args.socket_host, args.socket_port)
