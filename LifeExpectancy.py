import json
import numpy as np
import os
import statistics
from scipy import stats


MAX_MORTALITY_DISTRIBUTION_REFINEMENT_ITERATIONS = 20
# How close we need to get to the given target_life_expectancy
MORTALITY_DISTRIBUTION_REFINEMENT_TOLERANCE = 1/365

# Read the file that contains the parameters for the mortality distributions.
# Each item is a tuple containing the parameters (alpha, beta) of a beta
# distribution.
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "mortality_parameters.json")) as mortality_parameters_file:
	mortality_distributions = [stats.beta(alpha, beta, 0, 1) for alpha, beta in json.load(mortality_parameters_file)]

max_age = len(mortality_distributions)-1


def get_mortality_density(mortality_rates):
	still_alive = 1
	for mortality_rate in mortality_rates:
		yield still_alive * mortality_rate
		still_alive *= (1-mortality_rate)


def get_mortality_distribution_for_life_expectancy(living_age, target_life_expectancy):
	"""
	Determine the probability value for a set of death likelihood probability
	densities that leads to the given life expectancy
	Args:
		living_age (int): The age that an individual has made it to without dying
		target_life_expectancy (int): The life expectancy
	"""

	# Trim the life expectancy and data by the number of years already lived
	target_years_remaining = target_life_expectancy - living_age
	remaining_mortality_distributions = mortality_distributions[living_age:]

	# Start with the min and max possible probability values
	lower = 0
	upper = 1

	# Keep track of the number of iterations and loop until the max is reached
	iterations = 0
	while iterations < MAX_MORTALITY_DISTRIBUTION_REFINEMENT_ITERATIONS:

		# Choose the halfway point between the bounds we have so far narrowed down to
		mortality_percentile = statistics.mean([lower, upper])

		# Get the rates, density, and life expectancy for the percentile
		mortality_rates = [distribution.ppf(mortality_percentile) for distribution in remaining_mortality_distributions]
		mortality_density = list(get_mortality_density(mortality_rates))
		calculated_years_remaining = sum([index * value for index, value in enumerate(mortality_density)])

		# If we're within the tolerance, we can return
		miss = calculated_years_remaining - target_years_remaining
		if abs(miss) <= MORTALITY_DISTRIBUTION_REFINEMENT_TOLERANCE:
			break

		# Otherwise, narrow the range of percentiles that we will consider
		# based on which side we missed on
		if miss > 0:
			lower = mortality_percentile
		else:
			upper = mortality_percentile

		iterations += 1

	# TODO: The scaling may not be necessary assuming the distribution parameters
	#       are valid.
	# Scale the densities so remove any inaccuracy in the refinement and make sure
	# they add to 1
	return [p/sum(mortality_density) for p in mortality_density]
