import numpy as np
import unittest

import LifeExpectancy
import WealthExpectancy


phases = [
	{"age": 50, "contribution": 10000, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } },
	{"age": 65, "contribution": 0, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } },
	{"age": 90, "contribution": 0, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } }
]
age_phases = [
	(0, 50, {"age":50, "contribution": 10000, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } }),
	(1, 51, {"age":51, "contribution": 10000, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } }),
	(2, 52, {"age":52, "contribution": 10000, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } })
]


class TestWealthExpectancy(unittest.TestCase):


	def test_get_phases_by_age(self):

		phase0 = {"age": 50}
		phase1 = {"age": 65}
		phase2 = {"age": 90}
		i = phase0["age"]
		for ind, age, phase in WealthExpectancy.get_phases_by_age([phase0, phase1, phase2]):
			self.assertEqual(age, i)
			if age < phase1["age"]:
				self.assertEqual(phase, phase0)
			elif age < phase2["age"]:
				self.assertEqual(phase, phase1)
			elif age < LifeExpectancy.max_age:
				self.assertEqual(phase, phase2)
			i += 1

		self.assertEqual(i, LifeExpectancy.max_age)


	#def test_simulate_returns(self):
	#	returns = WealthExpectancy.simulate_returns(age_phases, randvars=[0, 0, 0])
	#	np.testing.assert_array_equal(returns, [1.06, 1.06, 1.04])


	def test_simulate_wealth(self):
		trivial_return_simulator = lambda age_phases : [age_phase[2]["average_return"] for age_phase in age_phases]
		net_worths = WealthExpectancy.simulate_wealth(200000, age_phases)
		self.assertEqual(len([i for i in net_worths]), 3)


	def test_simulate_wealth_multiple(self):
		net_worths = WealthExpectancy.simulate_wealth_multiple(1000, 200000, age_phases)
		self.assertEqual(net_worths.shape, (len(age_phases), 1000))
