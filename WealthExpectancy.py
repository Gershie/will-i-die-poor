import numpy as np
import scipy.special
import scipy.stats
import statsmodels.api as smapi

import LifeExpectancy


def get_phases_by_age(phases):
	"""
	Given a definition of phases, return a iterator whose values represent
	individual years and are tuples made up of (i) an index, (ii) the age, and
	(iii) the phase for the age
	"""
	start_ages = [phase["age"] for phase in phases]
	end_ages = [phase["age"] for phase in phases[1:]] + [LifeExpectancy.max_age]
	i = 0
	for phase, end_age in zip(phases, end_ages):
		for age in range(phase["age"], end_age):
			yield (i, age, phase)
			i += 1


RANDVAR_BATCH_SIZE = 100

def get_batch_generator(batch_provider):
	return (i for _ in iter(int, 1) for i in batch_provider())

def get_metropolis_hastings_generator(dist_func, proposer, initial=None, uniform_generator=None):
	current = initial
	if uniform_generator is None:
		uniform_generator = get_batch_generator(lambda : scipy.stats.uniform.rvs(size=RANDVAR_BATCH_SIZE))
	while True:
		candidate = proposer(current)
		rat = dist_func(candidate) / dist_func(current)
		rv = next(uniform_generator)
		if np.isnan(rat):
			print("current", current, "candidate", candidate, "uniform", rv, "ratio", rat, "good?", rat>rv)
		if rat > rv:
			current = candidate
			yield current
			continue


def get_gamma_proportional_pdf(a, loc, scale):
	# The proportional implementation below is potentially faster because it
	# excludes computing a scale but is prone to overflows. Since simulations
	# are no longer done at runtime we can use the stricted implementation without
	# worrying about efficiency.
	return lambda x : scipy.stats.gamma.pdf(x, a, loc, scale)

	"""
	# This approach was used for performance reasons but sometimes caused
	# overflows. Now that this code is no longer used at application run time
	# performance doesn't matter and the simpler, more reliable approach above
	# can be used.
	#
	# TODO: (If ever this code becomes useful again) Test this against scipy.stats
	def pdf_proportional(x):
		x_ = (x-loc)/scale
		s = scipy.special.xlogy(a-1.0, x_) - x_
		# Overflow exceptions can happen here when a is large. So far my solution has
		# been to fix a below 100.
		r = np.exp(s)
		import math
		if False and math.isinf(r):
			np.exp(495.7643964539308)
			print("a", a, "x", x, "loc", loc, "scale", scale, "x_", x_, "s", s, "r", r)
		return r
	return pdf_proportional
	"""


def get_metropolis_hastings_lognormal_generator(initial, proposal_scale, s, loc, scale):
	normals_generator = get_batch_generator(lambda : scipy.stats.norm.rvs(loc=0, scale=proposal_scale, size=RANDVAR_BATCH_SIZE))
	return get_metropolis_hastings_generator(
		lambda x : scipy.stats.lognorm.pdf(x, s, loc, scale),
		lambda prev : prev + next(normals_generator),
		initial=initial)
		


def get_metropolis_hastings_gamma_generator(initial, proposal_scale, a, loc, scale):
	"""
	Only a function proportional to the PDF is required so x^a * e^(-x) is used instead of the complete gamma distribution
	PDF. The complete function includes a denominator with an integral based on the parameter a, not the variable x, so
	it can be excluded.
	"""
	#print(initial, proposal_scale, a, loc, scale)
	normals_generator = get_batch_generator(lambda : scipy.stats.norm.rvs(loc=0, scale=proposal_scale, size=RANDVAR_BATCH_SIZE))
	return get_metropolis_hastings_generator(
		get_gamma_proportional_pdf(a, loc, scale),
		lambda prev : prev + next(normals_generator),
		initial=initial)


class SimulatedReturns(object):
	"""
	Calculates future wealths by simulating future returns at runtime
	"""

	def __init__(self, trials, returns_generator_provider):
		self.trials = trials
		self.returns_generator_provider = returns_generator_provider

	def get_wealth_grid(self, start_balance, age_phases, max_trials):
		trials = min(max_trials, self.trials)
		return simulate_wealth_multiple(trials, start_balance, age_phases, self.returns_generator_provider)


class CalculatedReturns(object):
	"""
	Calculates future wealths by using pre-determined future returns then carrying
	out simulations using vectorized computations
	""" 

	def __init__(self, returns):
		self.returns = returns

	def get_wealth_grid(self, start_balance, age_phases, max_trials):
		trials = min(max_trials, self.returns.shape[0])
		inflations = self.returns[-trials:, :len(age_phases), 0]
		s = np.array([[p[2]["asset_mix"][s] for s in ["developed", "emerging", "fixed", "cash"]] for p in age_phases])
		shares = np.repeat(np.expand_dims(s, axis=0), trials, axis=0)
		growths = np.sum(np.multiply(shares, self.returns[-trials:, :len(age_phases), 1:5]), axis=2)
		interests = self.returns[-trials:, 0:len(age_phases), 5]
		early_contributions = [age_phase[2]["contribution"] * (11/24 if age_phase[2]["contribution"] >= 0 else 13/24) for age_phase in age_phases]
		late_contributions = [age_phase[2]["contribution"] * (13/24 if age_phase[2]["contribution"] >= 0 else 11/24) for age_phase in age_phases]

		net_worths = np.empty((trials, len(age_phases)))
		previous_net_worths = np.repeat([start_balance], trials)
		for y in range(net_worths.shape[1]):
			black = previous_net_worths > 0
			y_returns = np.multiply(black, growths[:, y]) + np.multiply(~black, interests[:, y])
			net_worths[:, y] = np.multiply(previous_net_worths + early_contributions[y], y_returns + 1 - inflations[:, y]) + late_contributions[y]
			previous_net_worths = net_worths[:, y]

		return net_worths.T


class CalculatedReturnsSequential(object):
	"""
	Calculates future wealths by using pre-determined future returns then carrying
	out simulations sequentially
	""" 

	def __init__(self, returns, trials):
		self.returns = returns
		self.i = self.returns.shape[0]
		self.trials = trials

	def get_returns_generator(self):

		self.i = (self.i + 1) % self.returns.shape[0]
		return iter(tuple(self.returns[self.i, i, :]) for i in range(self.returns.shape[1]))

	def get_wealth_grid(self, start_balance, age_phases, max_trials):
		trials = min(max_trials, self.trials)
		return simulate_wealth_multiple(self.trials, start_balance, age_phases, self.get_returns_generator)


def get_returns_generator(parameters):

	developed_ratio_generator = get_batch_generator(
		lambda : scipy.stats.lognorm.rvs(**parameters["developed"]["distribution"], size=RANDVAR_BATCH_SIZE))

	emerging_to_developed_generator = get_batch_generator(
		lambda : scipy.stats.lognorm.rvs(**parameters["emerging-over-developed"]["distribution"], size=RANDVAR_BATCH_SIZE))

	inflation_ratio_generator = get_metropolis_hastings_lognormal_generator(
		**parameters["inflation"]["algorithm"],
		**parameters["inflation"]["distribution"])

	fixed_to_inflation_generator = get_batch_generator(
		lambda : scipy.stats.lognorm.rvs(**parameters["fixed-over-inflation"]["distribution"], size=RANDVAR_BATCH_SIZE))

	def get_configured_generator(inflation_ratio_generator, developed_ratio_generator, emerging_to_developed_generator, fixed_to_inflation_generator):
			previous_fixed = []
			while True:
					inflation_ratio = next(inflation_ratio_generator)
					fixed_ratio = inflation_ratio * next(fixed_to_inflation_generator)
					previous_fixed.append(fixed_ratio)
					if len(previous_fixed) > 5:
						previous_fixed.pop(0)

					developed_ratio = next(developed_ratio_generator)
					emerging_ratio = developed_ratio * next(emerging_to_developed_generator)
					
					yield (
							inflation_ratio - 1,
							developed_ratio - 1,
							emerging_ratio - 1,
							np.exp(np.log(np.product(previous_fixed))/len(previous_fixed)) - 1,
							max(0, (inflation_ratio-1)/2), # Cash equivalents is half of inflation, but not negative
							fixed_ratio - 1 + .015 # Borrowing is 1.5% greater than prime
					)
	return get_configured_generator(inflation_ratio_generator, developed_ratio_generator, emerging_to_developed_generator, fixed_to_inflation_generator)


def simulate_wealth(start_balance, age_phases, returns_generator_provider):
	"""
	Simulate changing wealth. The result is a list of net worths given a starting
	balance, phases for each year, and a method of simulating investment returns.
	Args:
		start_balance (int): The initial net worth
		age_phases (list of tuples): Describes investment parameters for each year
	"""
	net_worth = start_balance

	returns_generator = returns_generator_provider()

	for i, age, phase in age_phases:
		start_worth = net_worth
		inflation, developed_returns, emerging_returns, fixed, cash, borrowing = next(returns_generator)

		# Split contributions into a portion that are payed (or withdrawn) before returns are acknowledged and a portion
		# after. Assume earnings are slightly later in the year, expenses slightly earlier.
		earlier_contribution = phase["contribution"] * (5.5 if phase["contribution"] >= 0 else 6.5) / 12
		later_contribution = phase["contribution"] * (5.5 if phase["contribution"] < 0 else 6.5) / 12

		net_worth += earlier_contribution # Assume contributions and withdrawals keep up with inflation

		returns = -borrowing if net_worth < 0 \
			else sum([rate * phase["asset_mix"][share_name] for rate, share_name in
				[(developed_returns, "developed"), (emerging_returns, "emerging"), (fixed, "fixed"), (cash, "cash")]]
			) 

		net_worth *= (1 + returns - inflation)

		net_worth += later_contribution # Assume contributions and withdrawals keep up with inflation

		yield net_worth


def simulate_wealth_multiple(n, start_balance, age_phases, returns_generator_provider):
	"""
	Simulates changing wealth a number of times 
	"""
	return np.array(
		[[w for w in simulate_wealth(start_balance, age_phases, returns_generator_provider)] for _ in range(n)]
	).transpose()


def get_wealth_density_grid(wealth_grid, granularity):
	"""
	Given multiple wealth simulations, determine a density for each year across
	the simulations
	"""
	return [get_wealth_densities(wealth_grid[i, :], granularity) for i in range(wealth_grid.shape[0])]


def get_wealth_densities(data, granularity):
	"""
	Given observed wealth values, determine discrete density distribution
	Args:
		data (list of float): Observed values
		granularity (int): Number of discrete values to describe the density
	Returns:
		list of dict with entries "wealth" and "Probability" to describe values and
		their density
	"""
	kde = smapi.nonparametric.KDEUnivariate(data)
	kde.fit()
	low = min(kde.support)
	high = max(kde.support)
	margin = high-low
	step = (high-low) / (granularity+1)
	domain = [i*step+low for i in range(granularity+1)]
	return [{"wealth":x, "Probability":y} for x, y in zip(domain, kde.evaluate(domain))]


