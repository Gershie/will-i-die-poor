const NODE_TYPE_ELEMENT = 1;

/* Input and calculation */

function handleAssetBlur(element) {
	if (element.value.trim().length == 0) {
		element.value = "0";
	}
}

function updateCash(phase) {
	var remaining = 100 -
		[".developed", ".emerging", ".fixed"]
			.map(c => parseFloat(phase.select(c).node().value))
			.reduce((a, b) => a + b, 0);
	var rounding_factor = 1000000;
	phase.select(".cash").node().value = Math.round(remaining*rounding_factor)/rounding_factor;
}
function showSavingsAction(phase) {
	var positive = phase.select(".savingsaction").node().value == "+";
	phase.selectAll(".selectedsavingsaction .positive, .alternativesavingsaction .positive")
		.style("display", positive ? "" : "none");
	phase.selectAll(".selectedsavingsaction .negative, .alternativesavingsaction .negative")
		.style("display", positive ? "none" : "");
}
function swapSavingsAction(phase) {
	var savingsaction = phase.select(".savingsaction");
	savingsaction.node().value = savingsaction.node().value == "+" ? "-" : "+";
	showSavingsAction(phase);
}
function getInput() {
	input = {};
	input["initial_net_worth"] = document.getElementById("initial_net_worth").value;
	input["phases"] = getPhases();
	input["life_expectancy"] = document.getElementById("life_expectancy").value;
	return input;
}
function validate(input) {
	messages = [];
	if (isNaN(parseFloat(document.getElementById("initial_net_worth").value))) {
		messages.push("Enter a number for Initial Savings");
	}
	if (phases.length < 1) {
		messages.push("Enter at least one life phase");
	}
	var previous = NaN;
	d3.selectAll(".phases > div").each(function(d, i) {
		var phase = d3.select(this)
		var name = phase.select(".name").node().value;
		var suffix = " (Stage #" + (i+1) + (name ? ": " + name : "") + ")";

		var age_str = phase.select(".age").node().value;
		var age = parseInt(age_str);
		if (isNaN(age) || (i == 0 && age > exp) || (i > 0 && age <= previous)) {
			messages.push("Enter a whole number for age" + suffix);
		}

		var values = [ ".developed", ".emerging", ".fixed", ".cash" ]
			.map(c => parseFloat(phase.select(c).node().value));

		var anyNaN = values.map(c => isNaN(c))
			.reduce((a, b) => a || b, false);

		var anyNaPct = values.map(c => c < 0 || c > 100)
			.reduce((a, b) => a || b, false);

		if (anyNaN || anyNaPct) {
			messages.push("Enter percentages between 0 and 100 for asset classes" + suffix);
		}

		if (isNaN(parseFloat(phase.select(".contribution").node().value))) {
			messages.push("Enter a number for Contribution/Withdrawal" + suffix);
		}

		previous = age;
	});
	var exp_str = document.getElementById("life_expectancy").value;
	var exp = parseFloat(exp_str);
	if (isNaN(exp) || exp < 20 || exp > 100) {
		messages.push("Enter a number between 20 and 100 for Life Expectancy");
	}
	return messages;
}
function getData(input) {
	var request = new XMLHttpRequest();
	request.open("POST", "get_wealth_expectancy");
	request.onload = function(response) {
		if (this.status == 200) {
			display_data(input, JSON.parse(this.response));
		} else {
			alert("An unexpected error occurred. Status: " + this.status + "\n" + this.statusText);
		}
		d3.selectAll(".dialog, .working").style("display", "none");
	};
	request.onerror = function(e) {
		d3.selectAll(".dialog, .working").style("display", "none");
		alert("An unexpected error occurred\n" + (e.message ? e.message : ""));
	};
	var formData = new FormData();
	formData.append("initial_net_worth", input["initial_net_worth"]);
	formData.append("phases", JSON.stringify(input["phases"]));
	formData.append("life_expectancy", input["life_expectancy"]);
	request.send(formData);
}
function getPhases() {
	inputPhases = d3.selectAll(".phases > div");
	return inputPhases.nodes().map(function(d) {
		i = d3.select(d);
		return {
			"name": i.select(".name").node().value,
			"age": parseInt(i.select(".age").node().value),
			"asset_mix": {
				"developed": parseFloat(i.select(".developed").node().value) / 100,
				"emerging": parseFloat(i.select(".emerging").node().value) / 100,
				"fixed": parseFloat(i.select(".fixed").node().value) / 100,
				"cash": parseFloat(i.select(".cash").node().value) / 100,
			},
			"contribution": parseFloat(i.select(".contribution").node().value) * (i.select(".savingsaction").node().value == "+" ? 1 : -1),
		};
	});
}
function loadInput(input) {
	document.getElementById("initial_net_worth").value = input["initial_net_worth"];
	document.getElementById("life_expectancy").value = input["life_expectancy"];
	var phaseContainer = d3.select(".phases")
	phaseContainer.selectAll("*").remove();
	input["phases"].forEach(addPhase);
	phaseContainer = d3.select(".phases");
	d3.selectAll(".phases > div").data(input["phases"]).each(function (d) {
		var p = d3.select(this);
		p.select(".name").node().value = d["name"];
		p.select(".age").node().value = d["age"];
		var contribution = d["contribution"];
		p.select(".savingsaction").node().value = contribution >= 0 ? "+" : "-";
		p.select(".contribution").node().value = Math.abs(contribution);
		p.select(".developed").node().value = 100 * d["asset_mix"]["developed"];
		p.select(".emerging").node().value = 100 * d["asset_mix"]["emerging"];
		p.select(".fixed").node().value = 100 * d["asset_mix"]["fixed"];
		p.select(".cash").node().value = 100 * d["asset_mix"]["cash"];
		showSavingsAction(p);
	});
	updatePhaseSummaries();
}

/* Phase list */

function childElements() {
	return Array.from(this.childNodes).filter(n => n.nodeType == NODE_TYPE_ELEMENT);
}
function addPhase(name, args) {
	var template = d3.select(".template");
	var phases = d3.select(".phases");
	phases.node().insertAdjacentHTML("beforeend", template.html());
	var newPhase = phases.select(".phases > div:last-child");
	if (name) {
		newPhase.select(".name").node().value = name;
	}
	if (args && args.hasOwnProperty("savingsaction")) {
		newPhase.select(".savingsaction").node().value = args.savingsaction;
	}
	showSavingsAction(newPhase);
}
function calculate() {
	var input = getInput();
	var validation = validate(input);
	if (validation.length == 0) {
		hideShowDialog("working");
		getData(input);
	} else {
		showValidation(validation);
		hideShowDialog("validation");
	}
}
function togglePhase(phaseElement) {
	var wat = d3.select(phaseElement);
	wat.classed("closed", !wat.classed("closed"));
	updatePhaseSummaries();
}
function updatePhaseSummaries() {
	d3.select(".phases").selectAll(childElements).each(function(d, i) {
		var phase = d3.select(this);
		var mix = [".developed", ".emerging", ".fixed", ".cash"].map(c => Math.round(phase.select(c).node().value)).join("/");
		var age = phase.select(".age").node().value;
		var contribution = phase.select(".contribution").node().value * (phase.select(".savingsaction").node().value == "+" ? 1 : -1);
		phase.select(".stageindex").html(i+1);
		phase.select(".summary").html(
			"Age: " + (age === "" ? "?" : age)
			+ "<br />" + (contribution >= 0 ? "Contribution" : "Withdrawal") + ": $" + (contribution === "" ? "?" : numberWithCommas(Math.abs(contribution), 0))
			+ ", Asset mix: " + mix
		);
	});
}
function hideShowDialog(show) {
	if (show) {
		d3.selectAll(".dialog, ." + show).style("display", "")
	} else {
		d3.selectAll(".dialog, .validation, .working, .confirm").style("display", "none");
	}
}
function showValidation(validation) {
	var validationlist = d3.select("#validationlist")
	validationlist.selectAll("li").remove();
	validationlist.selectAll("li").data(validation).enter().append("li").text(function (d) { return d; });
}

/* Dataviz */

function getPercentageString(p) {
	var s;
	var p100 = p*100;
	if (p < .01) {
		s = p100.toFixed(1);
		if (s == "0.0") {
			return "no";
		} else if (s == "1.0") {
			s = "1";
		}
	} else {
		s = p100.toFixed(0);
	}
	return "a " + s + "%";
}

function drawLifeGraph(input, data) {
	// set the dimensions and margins of the graph
	var divLife = d3.select(".life");
	var containerNode = divLife.node();
	var chartLife = divLife.select("svg");
	var margin = {top: 0, right: 0, bottom: 0, left: 0};
	var width = chartLife.node().viewBox.baseVal.width - margin.left - margin.right;
	var height = chartLife.node().viewBox.baseVal.height - margin.top - margin.bottom;

	chartLife.selectAll("*").remove();
	var containerLife = chartLife
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	maxProb = d3.max(data["WealthExpectancy"], function(d) { return Math.max(d.DyingPoorProbability, d.DyingWealthyProbability); });
	maxProb = maxProb * 1.02; // Make sure the curve doesn't quite touch the border

	xAxisWidth = 20;
	xExtent = d3.extent(data["WealthExpectancy"], function(d) { return d.age; })
	var x = d3.scaleLinear()
		.domain(xExtent)
		.range([ xAxisWidth, width ]);

	var y = d3.scaleLinear()
		.domain([-maxProb, maxProb])
		.range([ height, 0 ]);

	// Add lines dividing the graph into sections for each of the phases
	var phases = input["phases"].map((item, index) => ({
		"name": item["name"],
		"start": item["age"],
		"end": index+1 < input["phases"].length ? input["phases"][index+1]["age"] : d3.max(data["WealthExpectancy"], year => year.age),
		"alternating": (input["phases"].length - index) % 2 == 0
	}));
	var phaseBackgrounds = containerLife.append("g");
	phaseBackgrounds.classed("divider", true);
	phaseBackgrounds.selectAll("line").data(phases).enter().append("line")
		.attr("x1", function(d, i) { return x(d["start"]); })
		.attr("x2", function(d, i) { return x(d["start"]); })
		.attr("y1", y(maxProb))
		.attr("y2", y(-maxProb));
	phaseBackgrounds.selectAll("text").data(phases).enter().append("text")
		.text(function(d) { return d["name"].toUpperCase(); })
		.attr("transform", function(d) { return "rotate(-90, " + (x(d["start"])-6) + ", " + y(-maxProb) + ")"; })
		.attr("x", function(d) { return x(d["start"])-4; })
		.attr("y", y(-maxProb)+14);

	// Add Y axis
	var g = containerLife.append("g");
	g.append("line")
		.attr("x1", x(xExtent[0]))
		.attr("y1", y(-maxProb))
		.attr("x2", x(xExtent[0]))
		.attr("y2", y(maxProb))
		.attr("stroke", "black");
	g.append("text")
		.attr("text-anchor", "end")
		.text("Likelier to die wealthy")
		.attr("transform", "rotate(-90, " + 0 + ", " + 0 + ")")
		.attr("x", 0)
		.attr("y", 16)
		.attr("fill", "black");
	g.append("text")
		.text("Likelier to die poor")
		.attr("transform", "rotate(-90, " + 0 + ", " + y(-maxProb) + ")")
		.attr("x", 0)
		.attr("y", y(-maxProb)+16)
		.attr("fill", "black");

	var up_down = data["WealthExpectancy"].map(d => ({ age : d.age, up : d.DyingWealthyProbability, down : -d.DyingPoorProbability }));
	// Smooth out the curve even though we know the first probabilities will always be 0
	if (up_down.length > 2) {
		if (up_down[0].up == 0) {
			up_down[0].up = 2*up_down[1].up - up_down[2].up;
		}
		if (up_down[1].down == 0) {
			up_down[0].down = 2*up_down[1].down - up_down[2].down;
		}
	}

	// Add the area
	containerLife.append("path")
		.datum(up_down)
		.classed("greenarea", true)
		.attr("d", d3.area()
			.x(function(d) { return x(d.age) })
			.y0(y(0))
			.y1(function(d) { return y(d.up) }))
	containerLife.append("path")
		.datum(up_down)
		.classed("greyarea", true)
		.attr("d", d3.area()
			.x(function(d) { return x(d.age) })
			.y0(y(0))
			.y1(function(d) { return y(d.down) }));
	var focus = containerLife.append("line")
		.classed("focus", true)
		.style("opacity", 0)
		.attr("y1", y(maxProb))
		.attr("y2", y(-maxProb));

	containerLife.append("text")
		.text("Age")
		.attr("text-anchor", "end")
		.attr("x", x(xExtent[1]))
		.attr("y", y(0)-4)
		.attr("fill", "black");
	// Add X axis
	containerLife.append("g")
		.attr("transform", "translate(0," + height/2 + ")")
		.classed("axis", true)
		.attr("font-size", "")
		.call(d3.axisBottom(x));

	function showSelection() {
		focus.transition().duration(250).style("opacity", 0.3);
		d3.selectAll(".ageselected").transition().duration(250).style("opacity", 1);
		d3.selectAll(".noageselected").transition().duration(250).style("opacity", 0);
	}

	function clearSelection() {
		focus.transition().duration(250).style("opacity", 0);
		d3.selectAll(".ageselected").transition().duration(250).style("opacity", 0);
		d3.selectAll(".noageselected").transition().duration(250).style("opacity", 1);
	}

	function updateSelection() { // touchstart, touchmove
		age = Math.round(x.invert(d3.mouse(this)[0]));
		yearData = data["WealthExpectancy"].filter(year => year.age == age)[0];
		poorLikelihood = yearData.DyingPoorProbability;
		wealthyLikelihood = yearData.DyingWealthyProbability;

		var ageX = x(age)
		focus.attr("x1", ageX)
			.attr("x2", ageX);

		die = poorLikelihood + wealthyLikelihood;
		broke = die ? (poorLikelihood / die) : 0;
		d3.selectAll(".spanSelectedAge").html(age.toFixed(0));
		d3.select("#spanSelectedDie").html(getPercentageString(die));
		d3.select("#spanSelectedPoor").html(getPercentageString(broke));

		var dead = data["WealthExpectancy"].filter(year => year.age < age)
			.map(year => year.DyingPoorProbability + year.DyingWealthyProbability)
			.reduce((a, c) => a + c, 0);
		var living = 1 - dead - die;
		var thisYearData = data["WealthExpectancy"].filter(year => year.age == age);
		var wealthy = thisYearData.length == 0 ? 1 :
			(thisYearData[0].DyingWealthyProbability + thisYearData[0].DyingPoorProbability) == 0 ? (yearData.MeanWealth > 0 ? 1 : 0) :
			thisYearData[0].DyingWealthyProbability / (thisYearData[0].DyingWealthyProbability + thisYearData[0].DyingPoorProbability);

		d3.select(".living span").html("" + (100 * living).toFixed(1) + "%");
		d3.select(".wealthy span").html("" + (100 * wealthy).toFixed(1) + "%");
		
		var wealthyPoint = Math.sin(2*Math.PI*wealthy+Math.PI).toFixed(4) + " " + Math.cos(2*Math.PI*wealthy+Math.PI).toFixed(4);
		var livingPoint = Math.sin(2*Math.PI*living+Math.PI).toFixed(4) + " " + Math.cos(2*Math.PI*living+Math.PI).toFixed(4);
		var topPoint = (0).toFixed(4) + " " + (0).toFixed(4);

		var pointForPortion = portion => Math.sin(2*Math.PI*portion+Math.PI).toFixed(4) + " " + Math.cos(2*Math.PI*portion+Math.PI).toFixed(4);
		var codeForPortion = portion => "M 0 0 L 0 -1 A 1 1 0 " + (portion > .5 ? "1" : "0") + " 0 " + pointForPortion(portion) + " z";

		d3.select(".wealthy path").attr("d", codeForPortion(wealthy));
		d3.select(".wealthy circle").style("fill-opacity",
			pointForPortion(wealthy) == pointForPortion(0) && (wealthy > .5) ? "1" : "0");
		d3.select(".living path").attr("d", codeForPortion(living));
		d3.select(".living circle").style("fill-opacity",
			pointForPortion(living) == pointForPortion(0) && (living > .5) ? "1" : "0");

		d3.select(".money").html(yearData.MeanPositiveWealth > 0 ? "$" + numberWithCommas(yearData.MeanPositiveWealth, 2) : "N/A");
		
		updateWealthGraph(yearData.WealthDistribution, yearData.MeanPositiveWealth);
	}

	containerLife.append("rect")
		.classed("hovertarget", true)
		.attr("x", xAxisWidth)
		.attr("width", width-xAxisWidth)
		.attr("height", height)
		.on("mouseover", showSelection)
		.on("touchstart", showSelection)
		.on("mouseout", clearSelection)
		.on("mousemove", updateSelection)
		.on("touchstart", updateSelection)
		.on("touchmove", updateSelection);
}

yWealth = null;
xWealth = null;
maxWorth = null;
function numberWithCommas(x, places) {
	var parts = x.toFixed(places).split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	return parts.join(".");
}

function drawWealthGraph(data) {
	dmn = [d3.min(data["WealthExpectancy"], function(d) { return -d.DyingPoorProbability; }), d3.max(data["WealthExpectancy"], function(d) { return d.DyingWealthyProbability; })];

	var divWealth = d3.select(".wealth");
	var containerNode = divWealth.node();
	var chartWealth = divWealth.select("svg");
	var margin = {top: 0, right: 0, bottom: 0, left: 15},
		width = 72 - margin.left - margin.right,
		height = 308 - margin.top - margin.bottom;

	chartWealth.selectAll("g").remove();
	var containerWealth = chartWealth
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	containerWealth.append("rect");

	// Add X axis
	xWealth = d3.scaleLinear()
		.domain([0, 1])
		.range([ 0, 10 ]);
	containerWealth.append("g")
		.attr("transform", "translate(0," + height + ")");

	// Add Y axis
	maxWorth = Math.max(data["NetWorth10"], -data["NetWorth10"], data["NetWorth90"], -data["NetWorth90"])
	yWealth = d3.scaleLinear()
		.domain([0, maxWorth])
		.range([ height, 0 ]);
	var currencySymbol = "$";
	containerWealth.append("g")
		.classed("axis", true)
		.attr("font-size", "")
		.call(d3.axisRight(yWealth).tickFormat(d3.format(currencySymbol + ".2s")))
		.call(g => g.select(".domain").remove());

	containerWealth
		.append("path")
		.attr("fill", "url(#greenToRed)");

	containerWealth.append("line")
		.classed("thermometertick", true)
		.attr("x1", xWealth(0)-1)
		.attr("x2", xWealth(0)-11);
}

function updateWealthGraph(d, m) {
	var maxWealth = d3.max(d, function(f) { return f.wealth; });
	var minWealth = d3.min(d, function(f) { return f.wealth; });
	d3.select(".wealth > svg > g > path")
		.attr("d", "M" + (xWealth(0)-2) + "," + yWealth(minWealth) + " L" + (xWealth(0)-2) + "," + yWealth(maxWealth) + " L" + (xWealth(0)-10) + "," + yWealth(maxWealth) + " L" + (xWealth(0)-10) + "," + yWealth(minWealth));
	d3.select(".wealth > svg > g > line")
		.attr("y1", yWealth(m))
		.attr("y2", yWealth(m));

	var stops = d3.select("#greenToRed").selectAll("stop").data(d);
	stops.enter().append("stop");
	stops
		.attr("offset", function(f) { return "" + (100 - (maxWealth-f.wealth) / (maxWealth-minWealth) * 100) + "%"; })
		.style("stop-opacity", function(f) { return Math.min(1, 1.5*f.Probability); })
		.classed("gradientgreen", f => f.wealth >= 0)
		.classed("gradientred", f => f.wealth < 0);
}

function display_data(input, data) {
	d3.select("#summaryLikelihood").style("opacity", 1);
	d3.select("#spanPoor").html(getPercentageString(data["DyingPoorLikelihood"]));

	drawLifeGraph(input, data);
	drawWealthGraph(data);

	d3.selectAll(".calculation").style("opacity", 1);
	d3.selectAll(".nocalculation").style("opacity", 0);
}
