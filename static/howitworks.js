// Current year and number of years before and after to display
var currentYear = new Date().getFullYear();
var YEARS = 60;

// Known columns in the data that will be retrieved
var data_columns = ["inflation", "developed", "emerging", "fixed", "cash", "borrowing"];

// Cached data
var allSimulatedData = null;
var allHistoricalData = null;

// Get/cache simulation data
function getSimulationData(handler) {
	if (allSimulatedData == null) {
		var request = new XMLHttpRequest();
		request.open("GET", "get_future_returns");
		request.onload = function(response) {
			if (this.status == 200) {
				allSimulatedData = JSON.parse(this.response);
				handler(allSimulatedData);
			} else {
				alert("An unexpected error occurred getting future returns. Status: " + this.status + "\n" + this.statusText);
			}
		};
		request.onerror = function(e) {
			alert("An unexpected error occurred getting future returns\n" + (e.message ? e.message : ""));
		};
		request.send(new FormData());
	} else {
		handler(allSimulatedData);
	}
}

// Get/cache historical data
function getHistoricalData(handler) {
	if (allHistoricalData == null) {
		var request = new XMLHttpRequest();
		request.open("GET", "static/historical.json");
		request.onload = function(response) {
			if (this.status == 200) {
				allHistoricalData = JSON.parse(this.response);
				handler(allHistoricalData);
			} else {
				alert("An unexpected error occurred getting past returns. Status: " + this.status + "\n" + this.statusText);
			}
		};
		request.onerror = function(e) {
			alert("An unexpected error occurred getting past returns\n" + (e.message ? e.message : ""));
		}
		request.send(new FormData());
	} else {
		handler(allHistoricalData);
	}
}

// Retrieve data and draw permanent elements of charts
window.addEventListener('load', (event) => {
	getSimulationData(function(simulatedData) {
		allSimulatedData = simulatedData;
		d3.selectAll(".of").html(allSimulatedData.length);
		getHistoricalData(function(historicalData) {
			d3.selectAll(".simulation").each(function (d, i) {
				var asset = this.parentNode.parentNode.id;
				drawChart(d3.select(this), historicalData[asset]);
			});
			drawPath(1);
		});
	});
});

// Common x-scale and one y-scale per asset
var scale_x = null;
var asset_scale_y = {};

// Draw the permanent elements of a chart
// chart - the chart to draw
// historical - historical data to draw permanently
function drawChart(chart, historical) {
	var asset = chart.node().parentNode.parentNode.id;

	// The visible historical data
	var values = Object.values(historical).slice(-YEARS);

	// Choose the range of y values on the vertical axis
	var round_factor = Math.max.apply(Math, values) > 20 ? 10 : 1;
	var y_range = [
		Math.floor(
			Math.max(
				-Math.max.apply(Math, values)/4,
				Math.min(-Math.max.apply(Math, values)/20, Math.min.apply(Math, values)))
		/round_factor)*round_factor/100,
		Math.ceil(
			Math.max.apply(Math, values)/
		round_factor)*round_factor/100
	]

	// Determine scales (x scale calculation is redundant over multiple charts)
	scale_x = d3.scaleLinear().domain([currentYear - YEARS, currentYear + YEARS]).range([10, 170]);
	scale_y = d3.scaleLinear().domain(y_range).range([90, 5]);
	asset_scale_y[asset] = scale_y;

	var svg = chart.select("svg");

	// Container for axes and labels
	var structure = svg.append("g");

	// x- and y-axes
	structure.append("line")
		.attr("x1", scale_x(currentYear-YEARS)-4)
		.attr("x2", scale_x(currentYear+YEARS)+4)
		.attr("y1", scale_y(0))
		.attr("y2", scale_y(0));
	structure.append("line")
		.attr("x1", scale_x(currentYear))
		.attr("x2", scale_x(currentYear))
		.attr("y1", scale_y(y_range[0]-4))
		.attr("y2", scale_y(y_range[1]+4));

	// x ticks
	structure.append("line")
		.attr("x1", scale_x(currentYear-YEARS))
		.attr("x2", scale_x(currentYear-YEARS))
		.attr("y1", scale_y(0))
		.attr("y2", scale_y(0)+4);
	structure.append("text")
		.attr("x", scale_x(currentYear-YEARS))
		.attr("y", scale_y(0)+12)
		.classed("year", true)
		.text(currentYear-YEARS);
	structure.append("line")
		.attr("x1", scale_x(currentYear+YEARS))
		.attr("x2", scale_x(currentYear+YEARS))
		.attr("y1", scale_y(0))
		.attr("y2", scale_y(0)+4);
	structure.append("text")
		.attr("x", scale_x(currentYear+YEARS))
		.attr("y", scale_y(0)+12)
		.classed("year", true)
		.text(currentYear+YEARS);

	// y ticks
	structure.append("line")
		.attr("x1", scale_x(currentYear))
		.attr("x2", scale_x(currentYear)+4)
		.attr("y1", scale_y(y_range[1]))
		.attr("y2", scale_y(y_range[1]));
	structure.append("text")
		.attr("x", scale_x(currentYear)+6)
		.attr("y", scale_y(y_range[1])+3)
		.text(((100*y_range[1]).toFixed(0))+"%");

	// past data
	pastData = Object.keys(historical).map(d => ({ year: d, data: historical[d]/100 }));
	var line = d3.line().x(d => scale_x(d.year)).y(d => scale_y(d.data));
	svg.select("path.past").attr("d", d => line(pastData));
}

// Draws the series in each graph selected by the index of the series in the
// full data set
// index - the index of the series to display
function drawPath(index) {

	// make sure we have the data
	getSimulationData(function(simulatedData) {

		// Get the 0-indexed series
		simulationData = simulatedData[index-1];

		// Get the appropriate data for each of the graphs
		var data = d3.selectAll(".asset").nodes().map(e => e.id).map(id => ({
				line: d3.line().x(d => scale_x(d.year)).y(d => asset_scale_y[id](d.data)),
				data: simulationData.map((d, i) => ({ year:currentYear+i, data:d[data_columns.indexOf(id)] })) }));

		// Bind the data to the graphs and animate
		var paths = d3.selectAll("svg path.data").data(data);
		paths
			.transition().duration(200).attr("d", d => d.line(d.data));
	});
}

// Updates the data displayed in all the charts by choosing new series of data
// determined by their index.
// change - an integer indicating how much to change the current index
function updateData(change) {
	// Get the index of the currently displayed index and the index to change to
	var index = d3.select(".index");
	var newIndex = (((allSimulatedData.length + index.html() - 1) + change) % allSimulatedData.length) + 1;

	// Update the text displaying the index
	d3.selectAll(".index").html(newIndex);
	d3.selectAll(".of").html(allSimulatedData.length);

	// Update the data
	drawPath(newIndex);
}
