import numpy as np
import unittest

import LifeExpectancy


class TestLifeExpectancy(unittest.TestCase):


	def test(self):

		start_age = 40
		counts = LifeExpectancy.get_mortality_distribution_for_life_expectancy(start_age, 80)
		self.assertEqual(len(counts), LifeExpectancy.max_age-start_age+1)
		
