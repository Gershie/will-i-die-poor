import unittest

import CombinedExpectancy
import LifeExpectancy


phases = [
	{"age": 50, "contribution": 10000, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } },
	{"age": 65, "contribution": 0, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } },
	{"age": 90, "contribution": 0, "asset_mix": { "emerging": .2, "developed": .5, "fixed": .3, "cash": 0 } }
]


class TestCombinedExpectancy(unittest.TestCase):


	def test_get_wealth_expectancy(self):
		wealth_expectancy = CombinedExpectancy.get_expectancy_calculations(200000, phases, 80)
		self.assertTrue("DyingPoorLikelihood" in wealth_expectancy)
		self.assertTrue("WealthExpectancy" in wealth_expectancy)
		self.assertEqual(len(wealth_expectancy["WealthExpectancy"]), LifeExpectancy.max_age-phases[0]["age"])
