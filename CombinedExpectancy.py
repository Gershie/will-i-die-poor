import argparse
import datetime
import json
import numpy as np
import pandas as pd
import random

import LifeExpectancy
import WealthExpectancy


WEALTH_DENSITY_GRANULARITY = 258
WEALTH_RANGE_HIGH_PERCENTILE = 95
WEALTH_RANGE_LOW_PERCENTILE = 5
WEALTH_DENSITY_HIGH_PERCENTILE = 90


def scale_wealth_density_grid(density_grid, inclusion_share):
	all_densities = [density["Probability"] for densities in density_grid for density in densities]
	max_ish_density = np.percentile(all_densities, inclusion_share)
	for densities in density_grid:
		for density in densities:
			density["Probability"] = density["Probability"] / max_ish_density


def get_expectancy_calculations(start_balance, phases, life_expectancy, simulator, max_trials=500):
	age_phases = [(i, age, phase) for i, age, phase in WealthExpectancy.get_phases_by_age(phases)]
	wealth_grid = simulator.get_wealth_grid(start_balance, age_phases, max_trials)
	means = np.mean(wealth_grid, axis=1)
	positive_means = np.ma.average(wealth_grid, axis=1, weights=(wealth_grid>=0))
	positive_means = [None if m else d for m, d in zip(positive_means.mask, positive_means.data)]
	mortality_distribution = LifeExpectancy.get_mortality_distribution_for_life_expectancy(age_phases[0][1], life_expectancy)
	wealthy_at_age_probabilities = [np.sum(wealth_grid[i, :] >= 0) / wealth_grid.shape[1] for i, _, _ in age_phases]
	wealth_density_grid = WealthExpectancy.get_wealth_density_grid(wealth_grid, WEALTH_DENSITY_GRANULARITY)
	scale_wealth_density_grid(wealth_density_grid, WEALTH_DENSITY_HIGH_PERCENTILE)
	return {
		"DyingPoorLikelihood": 1 - sum([death * wealthy for death, wealthy in zip(mortality_distribution, wealthy_at_age_probabilities)]),
		"NetWorth10": np.percentile(wealth_grid, WEALTH_RANGE_LOW_PERCENTILE),
		"NetWorth90": np.percentile(wealth_grid, WEALTH_RANGE_HIGH_PERCENTILE),
		"WealthExpectancy": [{
				"age": age_phases[0][1],
				"DyingWealthyProbability": 0,
				"DyingPoorProbability": 0,
				"WealthDistribution": [
					{ "wealth": start_balance-1, "Probability": 0 },
					{ "wealth": start_balance, "Probability": 1 },
					{ "wealth": start_balance+1, "Probability": 0 }
				],
				"MeanWealth": start_balance,
				"MeanPositiveWealth": None if start_balance < 0 else start_balance
			}]
			+
			[{
				"age": age_phase[1]+1,
				"DyingWealthyProbability": death_at_age_probability * wealthy_at_age_probability,
				"DyingPoorProbability": death_at_age_probability * (1-wealthy_at_age_probability),
				"WealthDistribution": wealth_densities,
				"MeanWealth": mean,
				"MeanPositiveWealth": mean_positive
			}
			for age_phase, death_at_age_probability, wealthy_at_age_probability, wealth_densities, mean, mean_positive
				in zip(age_phases, mortality_distribution, wealthy_at_age_probabilities, wealth_density_grid, means, positive_means)
		]
	}


def get_likelihood(scenario, returns):
	"""
	Gets the likelihood of dying poor in a scenario given a set of futures
	Args:
		scenarios - A scenario (including initial amount, life expectancy, and
		            phases)
		returns (np array) - future x years x returns
	"""

	simulator = WealthExpectancy.CalculatedReturns(returns)
	wealth_expectancy = get_expectancy_calculations(
		scenario["initial_amount"], scenario["phases"], scenario["life_expectancy"], simulator)

	return wealth_expectancy["DyingPoorLikelihood"]


def get_representative_returns(returns_models_parameters, simulation_evaluation_scenarios,
		future_years, candidate_future_count, future_subset_count, future_count, progress_handler=None):
	"""
	Generates a large set of future returns then selects a subset of those futures
	that is representative of the larger set based on calculations using given
	scenarios
	Args:
		returns_models_parameters - the parameters used for generating returns
		simulation_evaluation_scenarios - the scenarios used to perform calculations to evaluate futures
		future_years - the number of future years to simulate
		candidate_future_count - the number of futures to simulate
		future_subset_count - the number of subsets of futures to 
		future_count - the number of futures 
		progress_handler - function that handles progress reports 
	"""
	
	# Simulate the large set of candidates that subsets will be drawn from and tested against
	candidates = np.zeros((candidate_future_count, future_years, 6))
	for t in range(candidate_future_count):
		returns_generator = WealthExpectancy.get_returns_generator(returns_models_parameters)
		for y in range(future_years):
			candidates[t, y, :] = np.array(next(returns_generator))

		if progress_handler is not None and (t+1) % 500 == 0:
			progress_handler(t+1, "futures generated")

	progress_handler(None, "all futures generated")

	# Calculate the dying-poor likelihoods in the given scenarios over the
	# simulated futures
	targets = { i: get_likelihood(s, candidates) for i, s in enumerate(simulation_evaluation_scenarios) }

	# Choose subsets of all futures
	future_subsets = [candidates[np.array(sorted(random.sample(range(candidates.shape[0]), future_count))), :, :]
	                  for f in range(future_subset_count)]

	progress_handler(None, "calculating subset accuracy") 

	# Calculate the dying-poor likelihoods in the given scenarios over the
	# selected futures in each subset
	df = pd.DataFrame([(s, f, get_likelihood(scenario, future_subsets[f]))
                    for f in range(len(future_subsets))
	                  for s, scenario in enumerate(simulation_evaluation_scenarios)],
	                  columns=["case", "subset", "value"])

	progress_handler(None, "calculation complete")

	# Evaluate how representative each subset is by comparing the likelihoods
	# calculated using them with the likelihoods calculated using all futures 
	df["target"] = df.apply(lambda row: targets[row["case"]], axis=1)
	df["error"] = (df["target"] - df["value"]).abs()

	# Select the most representative subset and return it
	worsts = df.groupby("subset").agg({ "error": max }).reset_index()
	return future_subsets[worsts.idxmin()["error"]]


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("command", choices=["generate_representative_returns"])
	parser.add_argument("--years", required=False, type=int, default=100)
	parser.add_argument("--future_set_size", required=False, type=int, default=10000)
	parser.add_argument("--subset_count", required=False, type=int, default=100)
	parser.add_argument("--subset_size", required=False, type=int, default=250)
	parser.add_argument("--destination", required=False, default="simulated_returns.npy")
	args = parser.parse_args();

	if args.command == "generate_representative_returns":
		
		with open("simulated_returns.parameters.json") as parameters_file:
			returns_models_parameters = json.load(parameters_file)

		with open("calculation_scenarios.json") as scenarios_file:
			simulation_evaluation_scenarios = json.load(scenarios_file)

		simulated_returns = get_representative_returns(returns_models_parameters,
			simulation_evaluation_scenarios, args.years, args.future_set_size, args.subset_count, args.subset_size,
			lambda n, s: print(datetime.datetime.now().strptime("%Y-%m-%d, %H:%M:%S") + " {} {}".format(n, s) if n is not None else s))

		np.save(args.destination, simulated_returns)
